# Description

This project is to test [Zappa serverless python framework](https://github.com/Miserlou/Zappa).
This project is created with Python 3.6 and Pyramid.

# Commands

1. `zappa init` - command to init zappa project.
2. `zappa deploy dev` - command to deploy project stage.
3. `zappa update dev` - command to update project stage code.
